
package testRunners;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import tests.CalculatorTest1;
import tests.CalculatorTest2;
import tests.CalculatorTest3;

// Class running the tests, run is divided to 4 test classes - CalculatorTest1, CalculatorTest2,
// CalculatorTest3, CalculatorTest4
public class TestRunner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(CalculatorTest1.class, CalculatorTest2.class, CalculatorTest3.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());
    }
} 