package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CalculatorPage;

public class CalculatorTest4 {

    private WebDriver driver;


    @Before
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 1000);
        driver.get("https://web2.0calc.com/");
    }
    //Press history dropdown and assert that the list contains the 3 operations executed e.g. 35*999+(100/4)=, cos(pi),sqrt(81)
    @Test
    public void testCalculation() throws InterruptedException {

        CalculatorPage calculatorPage = new CalculatorPage(driver);

        Thread.sleep(1000);
        calculatorPage.acceptCookies();

        calculatorPage.checkHistory();
        Thread.sleep(1000);
        calculatorPage.assertHistory();
        Thread.sleep(1000);

    }
    @After
    public void tearDown() {
        driver.quit();
    }
}