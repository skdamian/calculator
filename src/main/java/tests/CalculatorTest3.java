package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CalculatorPage;

public class CalculatorTest3 {

    private WebDriver driver;


    @Before
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 1000);
        driver.get("https://web2.0calc.com/");
    }
    //3. Calculate sqrt(81) and assert assert the correct result 9.
    @Test
    public void testCalculation() throws InterruptedException {

        CalculatorPage calculatorPage = new CalculatorPage(driver);
        Thread.sleep(1000);
        calculatorPage.acceptCookies();

        calculatorPage.makeThirdCalculation();
        calculatorPage.clickEquals();
        Thread.sleep(1000);
        calculatorPage.assertEquals3();
        Thread.sleep(1000);

    }
    @After
    public void tearDown() {
        driver.quit();
    }
}