package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CalculatorPage;

public class CalculatorTest1 {

    private WebDriver driver;


    @Before
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 1000);
        driver.get("https://web2.0calc.com/");
    }
    //1. Make first calculation - Calculate 35*999+(100/4)= and assert the correct result 34990.
    @Test
    public void testCalculation() throws InterruptedException {

        CalculatorPage calculatorPage = new CalculatorPage(driver);

        Thread.sleep(1000);
        calculatorPage.acceptCookies();
        calculatorPage.makeFirstCalculation();
        calculatorPage.clickEquals();
        Thread.sleep(1000);
        calculatorPage.assertEquals1();
        Thread.sleep(1000);

    }
    @After
    public void tearDown() {
        driver.quit();
    }
}