package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

// Main calss of the program - Calculator Page, main methods of the program are here
public class CalculatorPage {
    private WebElement Calculate;

    private WebDriver driver;
    public WebDriverWait wait;


    //*********Page Variables*********
    String baseURL = "https://web2.0calc.com/";
    String inputVal;

    //*********Web Elements*********
    By cookies = By.xpath("//*[@id=\"cookiesaccept\"]");
    By number3 = By.xpath("//*[@id=\"Btn3\"]");
    By number5 = By.xpath("//*[@id=\"Btn5\"]");
    By multiply = By.xpath("//*[@id=\"BtnMult\"]");
    By number9 = By.xpath("//*[@id=\"Btn9\"]");
    By add = By.xpath("//*[@id=\"BtnPlus\"]");
    By paranL = By.xpath("//*[@id=\"BtnParanL\"]");
    By number1 = By.xpath("//*[@id=\"Btn1\"]");
    By number0 = By.xpath("//*[@id=\"Btn0\"]");
    By divide = By.xpath("//*[@id=\"BtnDiv\"]");
    By number4 = By.xpath("//*[@id=\"Btn4\"]");
    By paranR = By.xpath("//*[@id=\"BtnParanR\"]");
    By equals = By.xpath("//*[@id=\"BtnCalc\"]");

    By rad = By.xpath("//*[@id=\"trigorad\"]");
    By pi = By.xpath("//*[@id=\"BtnPi\"]");
    By cos = By.xpath("//*[@id=\"BtnCos\"]");

    By number8 = By.xpath("//*[@id=\"Btn8\"]");
    By sqrt = By.xpath("//*[@id=\"BtnSqrt\"]");
    By clear = By.xpath("//*[@id=\"BtnClear\"]");
    By history = By.xpath("//*[@id=\"hist\"]");

    //Go to Calculator
    public CalculatorPage goToN11 (){
        driver.get(baseURL);
        return this;
    }


    public void click (By elementBy) {

        driver.findElement(elementBy).click();
    }






    public CalculatorPage acceptCookies(){

        click(cookies);
        return this;
    }

    public CalculatorPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //1. Make first calculation - Calculate 35*999+(100/4)= and assert the correct result 34990.
    public void makeFirstCalculation(){
        click(number3);
        click(number5);
        click(multiply);
        click(number9);
        click(number9);
        click(number9);
        click(add);
        click(paranL);
        click(number1);
        click(number0);
        click(number0);
        click(divide);
        click(number4);
        click(paranR);

    }
    //2. Calculate cos(pi) with the rad radio button and assert the correct result -1.
    public void makeSecondCalculation(){
        click(rad);
        click(pi);
        click(cos);

    }
    //3. Calculate sqrt(81) and assert assert the correct result 9.
    public void makeThirdCalculation(){
        click(number8);
        click(number1);
        click(sqrt);
    }
    //Press history dropdown and assert that the list contains the 3 operations executed e.g. 35*999+(100/4)=, cos(pi),sqrt(81)
    public void checkHistory(){
        click(number8);
        click(number1);
        click(sqrt);
    }

    public void clickEquals() throws InterruptedException {
        click(equals);
        Thread.sleep(1000);
        inputVal = driver.findElement(By.id("input")).getAttribute("value");
        System.out.println(inputVal);
        click(clear);
        Thread.sleep(500);
    }
    //Methods that assert correct results
    public void assertEquals1() throws InterruptedException {
        Assert.assertEquals("34990",inputVal );
        Thread.sleep(500);
    }

    public void assertEquals2() throws InterruptedException {
        Assert.assertEquals("-1",inputVal );
        Thread.sleep(500);
    }

    public void assertEquals3() throws InterruptedException {
        Assert.assertEquals("9",inputVal );
        Thread.sleep(500);
    }

    public void assertHistory() throws InterruptedException {
        click(history);
        Thread.sleep(1000);
        String inputVal =driver.findElement(By.xpath("//*[@id=\"histframe\"]/ul/li[1]/p[2]")).getText();
        String inputVal2 =driver.findElement(By.xpath("//*[@id=\"histframe\"]/ul/li[2]/p[2]")).getText();
        String inputVal3 =driver.findElement(By.xpath("//*[@id=\"histframe\"]/ul/li[3]/p[2]")).getText();
        System.out.println(inputVal);
        System.out.println(inputVal2);
        System.out.println(inputVal3);

        Assert.assertEquals("sqrt(81)",inputVal );
        Assert.assertEquals("cos(pi)",inputVal2 );
        Assert.assertEquals("35*999+(100/4)",inputVal3 );

        Thread.sleep(500);
    }

}
